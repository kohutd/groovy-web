---
home: true
heroText:
heroImage: /images/logo_big.png
tagline: ' '
---

<header class="hero">
	<p class="description">
		A multi-faceted language for the Java platform
    </p>
	<p style="text-align: center">
	  <b><a href="https://apache.org">Apache</a> Groovy</b> is a <b>powerful, optionally typed</b> and <b>dynamic</b> language, with <b>static-typing and static compilation</b> capabilities, for the Java platform aimed at improving developer productivity thanks to a concise, <b>familiar and easy to learn syntax</b>. It integrates smoothly with any Java program, and immediately delivers to your application powerful features, including scripting capabilities, <b>Domain-Specific Language</b> authoring, runtime and compile-time <b>meta-programming</b> and <b>functional programming</b>.
	</p>
	<p class="action">
		<router-link to="/documentation" class="nav-link action-button">Get Started →</router-link>
	</p>
</header>


<div class="features">
  <div class="feature">
    <h2><h1>📖</h1> Flat learning curve</h2>
    <p>Concise, readable and expressive syntax, easy to learn for Java developers</p>
  </div>
  <div class="feature">
    <h2><h1>⚙️</h1> Powerful features</h2>
    <p>Closures, builders, runtime & compile-time meta-programming, functional programming, type inference, and static compilation</p>
  </div>
  <div class="feature">
    <h2><h1>💞</h1>Smooth Java integration</h2>
    <p>Seamlessly and transparently integrates and interoperates with Java and any third-party libraries</p>
  </div>
  <div class="feature">
    <h2><h1>🔩</h1>Domain-Specific Languages</h2>
    <p>Flexible & malleable syntax, advanced integration & customization mechanisms, to integrate readable business rules in your applications</p>
  </div>
  <div class="feature">
    <h2><h1>🛠</h1>Vibrant and rich ecosystem</h2>
    <p>Web development, reactive applications, concurrency / asynchronous / parallelism library, test frameworks, build tools, code analysis, GUI building</p>
  </div>
  <div class="feature">
    <h2><h1>📝</h1>Scripting and testing glue</h2>
    <p>Great for writing concise and maintainable tests, and for all your build and automation tasks</p>
  </div>
</div>