---
sidebar: auto
---

# Install Groovy

## :inbox_tray: Download
In this download area, you will be able to download the distribution (binary and source), the Windows installer and the documentation for **Groovy**.

For a quick and effortless start on Mac OSX, Linux or Cygwin, you can use [SDKMAN!](http://sdkman.io/) (The Software Development Kit Manager) to download and configure any Groovy version of your choice. Basic [instructions](#sdkman) can be found below.

### - Stable
- **Download zip**: <SimpleLink :href="`https://bintray.com/artifact/download/groovy/maven/apache-groovy-binary-${$site.themeConfig.groovyVersion}.zip`">Binary Release</SimpleLink> | <SimpleLink :href="`https://bintray.com/artifact/download/groovy/maven/apache-groovy-src-${$site.themeConfig.groovyVersion}.zip`">Source Release</SimpleLink>
- **Download documentation**: <SimpleLink :href="`https://bintray.com/artifact/download/groovy/maven/apache-groovy-docs-${$site.themeConfig.groovyVersion}.zip`">JavaDoc and zipped online documentation</SimpleLink>
- **Combined binary / source / documentation bundle**: <SimpleLink :href="`https://bintray.com/artifact/download/groovy/maven/apache-groovy-sdk-${$site.themeConfig.groovyVersion}.zip`">Distribution bundle</SimpleLink>
    
You can learn more about this version in the release notes or in the changelog.

If you plan on using invokedynamic support, read those notes.


### - Snapshots
For those who want to test the very latest versions of Groovy and live on the bleeding edge, you can use our [snapshot builds](https://oss.jfrog.org/oss-snapshot-local/org/codehaus/groovy). As soon as a build succeeds on our continuous integration server a snapshot is deployed to Artifactory’s OSS snapshot repository.


### - Prerequisites
Groovy 2.5 requires Java 6+ with full support up to Java 8. There are currently some known issues for some aspects when using Java 9 snapshots. The `groovy-nio` module requires Java 7+. Using Groovy’s invokeDynamic features require Java 7+ but we recommend Java 8.

The Groovy CI server is also useful to look at to confirm supported Java versions for different Groovy releases. The test suite (getting close to 10000 tests) runs for the currently supported streams of Groovy across all the main versions of Java each stream supports.




## :cloud: Maven Repository
If you wish to embed Groovy in your application, you may just prefer to point to your favourite maven repositories or the [JCenter maven repository](https://oss.jfrog.org/oss-release-local/org/codehaus/groovy).

### - Stable Release

<MavenTable version="2.5.6"/>

To use the [InvokeDynamic](http://docs.groovy-lang.org/latest/html/documentation/invokedynamic-support.html) version of the jars just append ':indy' for Gradle or <classifier>indy</classifier> for Maven.



## :rainbow: SDKMAN!
This tool makes installing Groovy on any Bash platform (Mac OSX, Linux, Cygwin, Solaris or FreeBSD) very easy.

Simply open a new terminal and enter:
```bash
curl -s get.sdkman.io | bash
```
Follow the instructions on-screen to complete installation.

Open a new terminal or type the command:
```bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
```

Then install the latest stable Groovy:
```bash
sdk install groovy
```

After installation is complete and you’ve made it your default version, test it with:
```bash
groovy -version
```

That’s all there is to it!




## Other ways to get Groovy

### Installation on Mac OS X
#### MacPorts
If you’re on MacOS and have MacPorts installed, you can run:
```bash
sudo port install groovy
```
#### Homebrew
If you’re on MacOS and have Homebrew installed, you can run:
```bash
brew install groovy
```

### Installation on Windows
If you’re on Windows, you can also use the [NSIS Windows installer](http://docs.groovy-lang.org/latest/html/documentation/TODO-Windows+NSIS-Installer).

### Other Distributions
You may download other distributions of Groovy from [this site](https://bintray.com/groovy/maven).

### Source Code
If you prefer to live on the bleeding edge, you can also grab the [source code from GitHub](https://github.com/apache/groovy).

### IDE plugin
If you are an IDE user, you can just grab the latest [IDE plugin](http://docs.groovy-lang.org/latest/html/documentation/tools-ide.html) and follow the plugin installation instructions.




## Install Binary
These instructions describe how to install a binary distribution of **Groovy**.

- First, [Download](#download) a binary distribution of Groovy and unpack it into some file on your local file system.
- Set your `GROOVY_HOME` environment variable to the directory you unpacked the distribution.
- Add `GROOVY_HOME/bin` to your `PATH` environment variable.
- Set your `JAVA_HOME` environment variable to point to your JDK. On OS X this is `/Library/Java/Home`, on other unixes its often `/usr/java` etc. If you’ve already installed tools like Ant or Maven you’ve probably already done this step.

You should now have Groovy installed properly. You can test this by typing the following in a command shell:
```bash
groovysh
```
Which should create an interactive groovy shell where you can type Groovy statements. Or to run the [Swing interactive console](http://groovy-lang.org/install.html#..\..\../subprojects/groovy-console/src/spec/doc/groovy-console.adoc) type:
```bash
groovyConsole
```
To run a specific Groovy script type:
```bash
groovy SomeScript
```