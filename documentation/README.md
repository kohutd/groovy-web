---
sidebar: auto
---

# Documentation
The documentation is available as a [single-page document](http://groovy-lang.org/single-page-documentation.html), or feel free to pick at a direct section below.

You can also browse [documentation for other versions](http://groovy-lang.org/documentation.html#all-versions).

## :mortar_board: Getting Started
- [Download](/download/)
- [Install Groovy](/install/)
- [Differences with Java](/differences/)
- [The Groovy Development Kit](http://groovy-lang.org/groovy-dev-kit.html)
- [Runtime and compile-time metaprogramming](http://groovy-lang.org/metaprogramming.html)
- [The Grape dependency manager](http://groovy-lang.org/grape.html)
- [Testing guide](http://groovy-lang.org/testing.html)
- [Domain-Specific Languages](http://groovy-lang.org/dsls.html)
- [Integrating Groovy into applications](http://groovy-lang.org/integrating.html)
- [Security](http://groovy-lang.org/security.html)
- [Design patterns in Groovy](http://groovy-lang.org/design-patterns.html)
- [Style guide](http://groovy-lang.org/style-guide.html)

## :book: Language Specification
- [Syntax](http://groovy-lang.org/syntax.html)
- [Operators](http://groovy-lang.org/operators.html)
- [Program structure](http://groovy-lang.org/structure.html)
- [Object orientation](http://groovy-lang.org/objectorientation.html)
- [Closures](http://groovy-lang.org/closures.html)
- [Semantics](http://groovy-lang.org/semantics.html)

## :package: Groovy module guides
- [Parsing and producing JSON](http://groovy-lang.org/json.html)
- [Working with a relational database](http://groovy-lang.org/databases.html)
- [Processing XML](http://groovy-lang.org/processing-xml.html)
- [Scripting Ant tasks](http://groovy-lang.org/scripting-ant.html)
- [Template engines](http://groovy-lang.org/templating.html)
- [Creating Swing UIs](http://groovy-lang.org/swing.html)
- [Servlet support](http://groovy-lang.org/servlet.html)
- [Working with JMX](http://groovy-lang.org/jmx.html)

## :hammer_and_wrench: Tools
- [groovyc — the Groovy compiler](http://groovy-lang.org/groovyc.html)
- [groovysh — the Groovy command -like shell](http://groovy-lang.org/groovysh.html)
- [groovyConsole — the Groovy Swing console](http://groovy-lang.org/groovyconsole.html)
- [IDE integration](http://groovy-lang.org/ides.html)

## :books: API documentation
- [GroovyDoc documentation of the Groovy APIs](http://groovy-lang.org/api.html)
- [The Groovy Development Kit enhancements](http://groovy-lang.org/gdk.html)

## :star: Documentation for all Groovy versions
You can browse the documentation of a particular version of Groovy (since Groovy 1.7):

<SelectGroovyVersion/>