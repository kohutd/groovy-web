module.exports = {
	base: '/groovy-web/',
	title: 'Apache Groovy',
	themeConfig: {
		groovyVersion: '2.5.6',
		title: 'Apache Groovy',
		nav: [
			{
				text: 'GitHub', link: 'https://github.com/apache/groovy',
			}
		],
		sidebarDepth: 2,
		sidebar: {
			'/download/': [
				'',
				'os-install',
				'build-tools',
				'requirements',
			],
			// '/install/': [
			// 	'',
			// 	'maven',
			// ]
		}
	}
}
