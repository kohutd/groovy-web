# System requirements

| Groovy        | JVM Required (non-indy) |	JVM Required (indy) * |
| ------------- | ----------------------- | --------------------- |
| 3.0 - current | 1.8+                    |	1.8+                  |
| 2.5 - 2.6     | 1.7+                    |	1.7+                  |
| 2.3 - 2.4     | 1.6+                    |	1.7+                  |
| 2.0 - 2.2     | 1.5+                    |	1.7+                  |
| 1.6 - 1.8     | 1.5+                    |	N/A                   |
| 1.5           | 1.4+                    |	N/A                   |
| 1.0           | 1.4-1.7                 |	N/A                   |

> If you plan to use invoke dynamic support, please read the [support information](http://groovy-lang.org/indy.html).