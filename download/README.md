# Download

Ways to get Apache Groovy:

- Download a source or binary [distribution](#distributions).
- Use a package manager or bundle for your operating system.
- Refer to the appropriate Apache Groovy jars from your build tools.
- Grab the latest plugin for your IDE and follow the installation instructions.
- Find the latest source code in the Git repo (or the GitHub mirror).
- If you're using Docker, Groovy is available on Docker Hub.

<DownloadGroovy :version="$site.themeConfig.groovyVersion"/>
<br>
<br>

## :incoming_envelope: Distributions
Distributions are bundles of source or class files needed to build or use Groovy.

All Apache projects provide a source zip which lets anyone build the software from scratch. If any doubt arises, you can regard the source zip as the authoritative artifact for each release. We also provide binary, downloadable documentation and SDK (combines src, binary and docs) convenience artifacts. You can also find a link to a non-ASF Windows installer convenience executable (if available).

### :key: Verification
We provide OpenPGP signatures ('.asc') files and checksums for every release artifact. We recommend that you [verify](https://www.apache.org/info/verification.html) the integrity of downloaded files by generating your own checksums and matching them against ours and checking signatures using the [KEYS](https://www.apache.org/dist/groovy/KEYS) file which contains the OpenPGP keys of Groovy's Release Managers across all releases.

## :star: Groovy 3.0
Groovy 3.0 is a bleeding edge [version](http://groovy-lang.org/versioning.html) of Groovy designed for JDK8+ and with the new Parrot parser enabled by default. Pre-stable versions are available:
### - 3.0.0-alpha-4 distributions
<Distribution version="3.0.0-alpha-4"/>


## :star: Groovy 2.6
Groovy 2.6 is designed for JDK7+ and supports the new Parrot parser (when enabled) but has been retired before reaching final release to focus on Groovy 3.0. Alpha versions are available to help people wanting to port towards Groovy 3.0 but who are stuck on JDK7. See links under "Other versions" for details.


## :star: Groovy 2.5
Groovy 2.5 is the latest stable [version](http://groovy-lang.org/versioning.html) of Groovy.
### - 2.5.6 distributions
<Distribution version="2.5.6"/>


## :star: Groovy 2.4
Groovy 2.4 is the previous stable [version](http://groovy-lang.org/versioning.html) of Groovy. Important: Releases before 2.4.4 weren't done under the Apache Software Foundation and are provided as a convenience, without any warranty.
### - 2.4.16 distributions
<Distribution version="2.4.16"/>

## Other versions
Downloads for all versions are hosted (and mirrored) in:
- Apache's [release mirrors](http://www.apache.org/dyn/closer.cgi/groovy/) and [archive repository](https://archive.apache.org/dist/groovy/).
- Bintray's [Groovy repository](http://bintray.com/groovy/). Register on Bintray to rate, review, and register for new version notifications. Or [browse](https://dl.bintray.com/groovy/maven/) all versions.

You can also read the changelogs for [all versions](http://groovy-lang.org/changelogs.html).

## Invoke dynamic support
Please read the [invoke dynamic support information](http://groovy-lang.org/indy.html) if you would like to enable indy support and are using Groovy on JDK 7+.