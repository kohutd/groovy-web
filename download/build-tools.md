---
sidebarDepth: 4
---


# Build tools

If you wish to add Groovy as a dependency in your projects, you can refer to the Groovy JARs in the dependency section of your project build file descriptor:

## Core
Just the core of Groovy without the modules*. Also includes jarjar'ed versions of Antlr, ASM, and an internal copy of needed CLI implementation classes.

### - Gradle
```groovy
complile 'org.codehaus.groovy:groovy:x.y.z'
```
### - Maven
```xml
<dependency>
	<groupId>org.codehaus.groovy</groupId>
	<artifactId>groovy</artifactId>
	<version>x.y.z</version>
</dependency>
```


## Module
`"$module"` stands for the different optional groovy *modules*. Example: `groovy-sql`.

### - Gradle
```groovy
complile "org.codehaus.groovy:groovy-$module:x.y.z"
```
### - Maven
```xml
<dependency>
	<groupId>org.codehaus.groovy</groupId>
    <artifactId>groovy-$module</artifactId>
    <version>x.y.z</version>
</dependency>
```

### Modules
- **2.4.X**: `ant`, `bsf`, `console`, `docgenerator`, `groovydoc`, `groovysh`, `jmx`, `json`, `jsr223`, `nio`, `servlet`, `sql`, `swing`, `test`, `templates`, `testng` and `xml`
- **2.5.0**: as above but excluding optional module `bsf` plus `cli-picocli`, `datetime`, `macro`, `test-junit5`. Optional modules: `bsf`, `dateutil`, `cli-commons`
- **2.5.1+**: as above but `groovy-jaxb` is moved to become optional


## Groovy All
Core plus all of the modules (excluding optional modules) according to the version *packaging scheme*.

### - Gradle
```groovy
complile 'org.codehaus.groovy:groovy-all:x.y.z'
```
### - Maven
```xml
<dependency>
	<groupId>org.codehaus.groovy</groupId>
    <artifactId>groovy-all</artifactId>
    <version>x.y.z</version>
    <type>pom</type> <!-- required JUST since Groovy 2.5.0 -->
</dependency>
```

### Packaging Scheme
- **2.4.X**: The core plus all the modules merged into one *"fat jar"*. Optional dependencies are marked as optional, so you may need to include some of the optional dependencies to use some features of Groovy, e.g. AntBuilder, GroovyMBeans...
- **2.5+**: A *"fat pom"* `groovy-all-x.y.z.pom` referring to the core plus all modules (excluding optional ones). In order to cater to the module system of Java 9+, the `groovy-all-x.y.z.jar` file is no longer available.


## Maven repositories
Groovy release jars are available from [Maven Central](http://repo1.maven.org/maven2/org/codehaus/groovy/) or [JCenter](http://jcenter.bintray.com/org/codehaus/groovy/).

Groovy snapshot jars are available from [JFrog OpenSource Snapshots repository](https://oss.jfrog.org/oss-snapshot-local/org/codehaus/groovy). Snapshots are not official releases but are provided to assist with integration testing leading up to an official release.