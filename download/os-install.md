# Package manager

[Installing](/install/) Apache Groovy from a distribution zip is not hard but if you don't want the hassle, consider the alternatives listed here.


## SDKMAN!
[SDKMAN!](http://sdkman.io/) is a tool for managing parallel versions of multiple Software Development Kits on most Unix-based systems:
```bash
$ sdk install groovy
```
Windows users: see the SDKMAN [install](https://sdkman.io/install) instructions for potential options.

## Homebrew
[Homebrew](http://brew.sh/) is "the missing package manager for macOS":
```bash
$ brew install groovy
```

## MacPorts
[MacPorts](http://www.macports.org/) is a system for managing tools on macOS:
```bash
$ sudo port install groovy
```

## Scoop
[Scoop](http://scoop.sh/) is a command-line installer for Windows inspired by Homebrew:
```bash
> scoop install groovy
```

## Chocolatey
[Chocolatey](https://chocolatey.org/) provides a sane way to manage software on Windows:
```bash
> choco install groovy
```

---

Linux/*nix users: you may also find Groovy is available using your preferred operating system package manager, e.g.: apt, dpkg, pacman, etc.

Windows users: consider also the Windows installer (see links above under Distributions).